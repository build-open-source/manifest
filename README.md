### Get repo projects
```bash
# Install Repo
#   Ref: https://mirrors.tuna.tsinghua.edu.cn/help/git-repo/
curl https://mirrors.tuna.tsinghua.edu.cn/git/git-repo -o repo
chmod +x repo
export REPO_URL='https://mirrors.tuna.tsinghua.edu.cn/git/git-repo'

# Init & Sync
#   a. HTTPS
repo init -u https://gitee.com/build-open-source/manifest.git && repo sync -j8
#   b. SSH
#repo init -u git@gitee.com:build-open-source/manifest.git -m ssh.xml && repo sync -j8
#   c. LAN Mirror
#repo init -u USER@IPADDR:/PATH/TO/REPO/ROOT/manifest.git -m default_lan.xml && repo sync -j8
```
